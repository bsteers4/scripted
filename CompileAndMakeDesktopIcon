#!/usr/bin/env bash

MinimumVersion="3.19"

# Get application directory and exe name and cd directory if needed
WD=${0%/*}
AppName=${WD##*/}
[ "$(pwd)" != "$WD" ] && cd "$WD"
[ -z "$Path" ] && eval $(cat ./.project|grep Path=)
[ -z "$Path" ] && Path="$AppName.gambas"

source ./.installtranslations

####### show error msg and exit
ErrorOut() {
 if [ -z $(which zenity) ]; then
   echo "$1"
 else
   zenity --error --text="$1" 2>/dev/null
 fi
 exit 1
}

if [ -z $(which zenity) ]; then
read-p "Zenity needs to be installed to show dialogs: install zenity? (y/n)" ANS
[ "${ANS}" != "y" ] && ErrorOut "need zenity for dialogs"
if [ !-z $(which apt) ]; then
sudo apt-get install zenity
elif [ !-z $(which dnf) ]; then
sudo dnf install zenity
elif [ !-z $(which zypper) ]; then
sudo zypper install zenity
elif [ !-z $(which pacman) ]; then
sudo pacman -S zenity
fi
fi

# Check a version string is less than or equal to another
# args <version to check>, <version to beat> , sets $CheckVersionPass to 1 (passed) or 0 (failed)
CheckVersion() {

CheckVersionPass=1
OIFS=$IFS
IFS='.'
  R1=(); for s in $1; do R1+=($s); done
  R2=(); for s in $2; do R2+=($s); done
  CNT=0;
  while [ $CNT -lt ${#R1[@]} ]; do
    if [ -z "${R1[$CNT]}" ]||[ -z "${R2[$CNT]}" ]; then break; fi
    if [ ${R1[$CNT]} -lt ${R2[$CNT]} ]; then
     CheckVersionPass=0; break;
   elif [ ${R1[$CNT]} -gt ${R2[$CNT]} ]; then
    break
  fi;
(( CNT++ ));
done
IFS=$OIFS

}

GetID() {
source /etc/os-release
[ ! -z "$ID_LIKE" ] && ID=${ID_LIKE% *}
}

FindRepositoryVersion() {
if [ "$ID" = "ubuntu" ]||[ "$ID" = "debian" ]; then
  RV=$(apt-cache show gambas3-runtime|grep Version|awk '{print $2}'); V=${V%-*}
elif [ "$ID" = "fedora" ]; then
  RV=$(dnf info -Cq gambas3-runtime|grep Version|awk '{print $3}')
elif [[ "$ID" = "opensuse"* ]]; then
  RV="0"
elif [[ "$ID" = "arch"* ]]; then
  RV=$(pacman -Ss gambas3-runtime| grep 3.| awk '{print $2}')
  RV=${RV%-*}
fi
}

GetID

eval $(cat ./.settings|grep Path=)

# get desktop path
DESK=$(xdg-user-dir DESKTOP)
# Check gambas is correct version.
echo -n "$T_CHECKG"

VersionPass=1
PackagePass=0

if [ ! -z $(which gbh3) ]; then
  V1=$(gbh3 -V)
  CheckVersion $V1 $MinimumVersion
else
VersionPass=0
V1="nothing"
echo "Gambas is not installed"
fi

PS="Passed"; [ $VersionPass -ne 1 ] && PS="Failed"

echo " Test $PS $MinimumVersion required $V1 is installed"

# List components from .project file and check all components are installed.
Packages=""
for s in $(cat .project| grep Component=); do Packages+=" ${s//Component=/}"; done

LIB="/usr/lib/gambas3";
[ ! -e "$LIB" ] && LIB="/usr/lib64/gambas3"
MISSING=0; PKGS=()

[ $VersionPass -eq 1 ] && echo  "$T_CHECK_COMP: $Packages...."
for s in $Packages; do
  if [ ! -e "$LIB/$s.component" ]; then
    MISSING=1
    [ $VersionPass -eq 1 ] && echo  -ne "? $s no                    \r"
    PKGS+=($s)
    else
    [ $VersionPass -eq 1 ] && echo  -ne "? $s yes                 \r"
  fi
  [ $VersionPass -eq 1 ] && sleep 0.05
done

PKS="${PKGS[@]}"

[ $MISSING -eq 0 ] && echo -e "\rAll components exist     " || echo ""

if [ $MISSING -eq 1 ]||[ -z $(which gbr3) ]; then
zenity --question --text="gambas must be installed or updated to version with components\n$PKS" --icon-name=dialog-warning --ok-label="upgrade gambas" --cancel-label="Cancel" 2>/dev/null

[ $? -ne 0 ] && ErrorOut "Gambas upgrade cancelled, cannot use this version of ScriptED"

TM=${XDG_CURRENT_DESKTOP,,}"-terminal"
  if [ -z $(which $TM) ]; then TM="x-terminal-emulator";
    if [ -z $(which $TM) ]; then TM="sensible-terminal";
      if [ -z $(which $TM) ]; then zenity --error --text="Error no terminal emulator"; exit 1; fi
    fi
  fi
  echo "" >/tmp/hold
  $TM -e "env MinimumVersion='$MinimiumVersion' env Packages='$PKS' ./.update-gambas.sh"

  Hold() {
    sleep 1
    [ -e "/tmp/hold" ] &&  Hold
  }

  Hold

  if [ -e /tmp/err ]; then
    ERR=$(cat /tmp/err)
    rm /tmp/err
    ErrorOut "Error, Could not update gambas\n$ERR"
  fi
fi

# ask what launcher icons to make
REP=$(zenity --list --height=250 --width=300 --checklist --text="Compile $AppName in\n$WD\n\nSelect launcher icons to create after compiling" --column=Enable --column="Make launcher type" "" "Make menu" "" "Make desktop icon" 2>/dev/null)
ERR=$?

if [ $ERR -eq 1 ]; then
  echo "User canceled"
  exit;
fi

DODTOP=0; DOMENU=0

if [ ! -z "$REP" ]; then
[  "${REP%|*}" == "Make menu" ] && DOMENU=1
[  "${REP#*|}" == "Make desktop icon" ] && DODTOP=1
fi

[ $[$DOMENU+$DODTOP] -eq 0 ] &&  ERR=1

cd "$WD"

# find what flags to pass gbc3 by reading the .project info
echo "Compiling the application $Path"

chmod +x ./.gambas-compile.sh
./.gambas-compile.sh

[ $ERR -eq 1 ] &&  exit

echo "Making launcher..."
# select launcher icon alternative actions.
OPTS=$(zenity --forms --text="Select Alternative Actions for the icon.(options with right click)" --add-combo="GUI Toolkits (select gtk/qt5/etc)" --combo-values="|Add All|Add Non-default" --add-combo="Gambas Project Edit (open in gambas IDE)" --combo-values="|Add Project Edit" 2>/dev/null)

if [ $? -eq 1 ]; then zenity --info --width=400 --text="Create icon canceled."; exit; fi

[ -z "$OPTS" ] && OPTS=" | "
GUIMODE="${OPTS%|*}"
EDTMODE="${OPTS#*|}"

if [[ $GUIMODE != " " ]]; then
echo -e "Use \"gb.gui\"\nPublic Sub Main()\nIf Args.Count = 1 Then\nPrint Env[\"GB_GUI\"]\nQuit\nEndif\nIf Args[1] = Env[\"GB_GUI\"] Then Quit 0\nQuit 1\nEnd\n" >/tmp/gui
GUIS=""
DEF=$(gbs3 /tmp/gui)
ACTS=""

for s in gtk gtk3 qt4 qt5 qt6;do
  if [ ! -e "/usr/lib/gambas3/gb.$s.component" ]; then continue; fi
  if [[ "$GUIMODE" != "Add All" ]]; then
    [[ "gb.$s" == "$DEF" ]] &&  continue
  fi
  GB_GUI=gb.$s gbs3 /tmp/gui gb.$s 2>/dev/null;
  if [ $? -eq 0 ]; then
    SS=${s^^}
    [ "$SS" == "GTK" ] && SS="GTK2"
    GUIS=$GUIS"RUN_$SS;"
  ACTS="$ACTS\n[Desktop Action RUN_$SS]\nName=Run with $SS\nExec=env GB_GUI=gb.$s '$WD/$Path'\n"
fi
done
fi

if [[ $EDTMODE != " " ]]; then
  GUIS=$GUIS"EDITG;"
  ACTS="$ACTS\n[Desktop Action EDITG]\nName=Edit with Gambas3\nExec=gambas3 '$WD'\n"
fi

if [ -z "$ACTS" ]; then
  echo "no actions to add"
  else
ACTS="Actions=${GUIS%*;}\n$ACTS"
echo "Adding Actions=${GUIS%*;}"
fi

# try to get icon from .project file or use default
IC=$(cat "$WD/.project" |grep Icon=)

if [ -z "$IC" ]; then Icon=".icon.png"; else Icon=${IC##*=}; fi

if [ $DOMENU -eq 1 ]; then
  RES=$(zenity --list --hide-header --text="Choose menu category." --height=300 --column=Category Accesory Graphics Internet Office Development Sound Video "System Tools" "Desktop settings" "System Settings")
fi
if [ $? -eq 1 ]|[ -z $RES ]; then
  zenity --info --width=400 --text="Menu item canceled."
  DOMENU=""
  CATS=""
fi

if [ "$RES" == "Accesory" ]; then CATS="Application;Utility"
  elif [ "$RES" == "Internet" ]; then CATS="Application;Network;Internet;Web"
  elif [ "$RES" == "Audio" ]; then CATS="Application;AudioVideo;Player;Audio"
  elif [ "$RES" == "Video" ]; then CATS="Application;AudioVideo;Player;Video"
  elif [ "$RES" == "System Tools" ]; then CATS="Application;System"
  elif [ "$RES" == "Desktop Settings" ]; then CATS="Application;Settings;DesktopSettings"
  elif [ "$RES" == "System Settings" ]; then CATS="Application;System;Settings"
  else
  CATS="$RES"
fi

[ ! -z "$CATS" ] && CATS="Categories=$CATS\n"

cp "$WD/$Icon" "/tmp/${AppName,,}.png"

TEXT="[Desktop Entry]\nType=Application
Name=$AppName
Icon=${AppName,,}
Exec=$WD/$Path
$CATS$ACTS"

for sz in 16 22 32 48 64 128; do
xdg-icon-resource install --size $sz --novendor --noupdate "/tmp/${AppName,,}.png"
done
xdg-icon-resource forceupdate
rm "/tmp/${AppName,,}.png"

MSG="All Done"
if [ $DODTOP -eq 1 ]; then
  echo -e "$TEXT" >"$DESK/$AppName.desktop"
  chmod +x "$DESK/$AppName.desktop"
  MSG="$MSG\n$XDG_DESKTOP_DIR/$AppName.desktop created."
fi
if [ $DOMENU -eq 1 ]; then
  echo -e "$TEXT" >"$HOME/.local/share/applications/$AppName.desktop"
  MSG="$MSG\n$RES menu created."
  #chmod +x "$HOME/.local/share/applications/$AppName.desktop"
fi

zenity --info --width=400 --text="$MSG"
