#!/usr/bin/env bash

ControlPublic=0; ModulePublic=0;
eval $(cat ./.project | grep "ControlPublic") 2>/dev/null;
eval $(cat ./.project | grep "ModulePublic") 2>/dev/null;

KeepDebugInfo=1; eval $(cat .project|grep KeepDebugInfo);

FLG="-wax"; MODE="-f";
CHK=$(gbc3 --help| grep public-module| awk {'print $1'});
if [[ $CHK != "-f" ]]; then MODE="-"; fi;
if [ $KeepDebugInfo -eq 1 ]; then FLG="$FLG"g; fi;
if [ $ControlPublic -eq 1 ]; then FLG="$FLG "$MODE"public-control"; fi;
if [ $ModulePublic -eq 1 ]; then FLG="$FLG "$MODE"public-module"; fi;

gbc3 $FLG; gba3
