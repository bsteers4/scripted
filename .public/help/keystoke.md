
##The Keystroke recorder.

With ScriptEd it is possible to record your keystrokes when you type and then play the sequence back.

Keystrokes are recorded with any modifiers (Shift, Alt, Ctrl)

Supported Keys...
* Any text you type
* Arrow keys.
* F3 , for searching the next text from the last search.

This can be very usefull for adjust many lines of text where a simple search&replace will not do it.

By holding Shift or Alt when using arrow keys words can be skipped, selected and deleted.

To use either open the Keystroke panel and use the buttons or use the menu shortcuts..

* Ctrl-F7 , Start / Stop recording keystrokes.
* F7 , playback the strokes.

Tips..

* Use Ctrl-F to find some text. then from that position start recording the keystrokes and finish by pressing F3. Then the cursor will be in position at the next text occurence and ready to play the keys again if needed.

* Modify multiple lines by finishing the keystroke moves with pressing Home and Down key.  that moves the cursor to the start of the next line.
