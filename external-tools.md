## External tool scripts

 External tools are commands or scripts that you can run on either selected text of the whole document.

 Clicking the icon at the bottom of the editor will open the External Tools panel.

Initially it is in "menu" mode just showing available tools. You can double click items there to run them.

To Add/Remove/Edit items click the "Edit" icon in the top right of the Tools list to show the editing panels.

You can add and remove submenu and menu items in the left panel. you can also arrange their order.

To the right is the command panel.

 Before commands :  (*operations before runnig the tool*)
* Select to save file before executing the command.
* Select to run command in in a terminal.

Command(s):
* Enter a command or a full script here

After commands:  (*what to do with the result*)
* Paste into document at cursor position.
* Replace entire document.
* Do nothing with the document.

* Copy result to clipboard

Tokens can be inserted into the script that are replaced as follows...
* $SE_FILEPATH: Full File path
* $SE_FILENAME: The file name
* $SE_DIRECTORY: The files Directory
* $SE_SELECTED: Selected text
* $SE_ALLTEXT: All Text
These can be auto-inserted by selecting them in the Tokens list.
