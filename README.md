# ScriptED 3.0

Simple Linux shell script editor with a few features.

A Work in progress...\
But it is functional now so uploading it here for anyone else to examine/use/upgrade/whatever.

I have made this to help **me** with making shell scripts.

Written in Gambas Basic http://gambas.sourceforge.net/\
gambas3 needs to be installed.\

Main Features..

* Lists all script functions in a convenient list you can click the function names and the editor jumps to the function.
* Uses the Gambas3 TextEditor object that has many handy features.
* custom colour coding, highlight theme saved individually for each filetype mode.
* has a Run button to save and launch the loaded work script in a TerminalView object.
* Open multiple document tabs.
* Single instance launching. drag-n-dropping multiple files onto the launcher will open ap once with files in tabs.

* Hand built Keyboard Macro recorder (basic and experimental), record and playback keystrokes.
   (i used it's own macro recorder to format the above component list, converted 
   Component=gb.args\n into gambas3-gb.args and all the others in seconds :) )

* External tools , create a menu of custom functions , functions can be any script.

* Teminal tabs

Like I say this has been made to help me when making shell scripts.

Thanks to all at the gambas mailing list for any help.
Special thanks to Benoit Minisini for gambas and any of his code i have copied.
## Compile / Install..
Run CompileAndMakeDesktopIcon script to compile the executable and make lanchers in system menu and/or on the desktop.\
The launchers will point to wherever the scripted folder is when the script is run.\
(you may need to set the scripts executable flag to doubleclick-run it)

## Uninstall...
Simply delete the project folder, then the residual settings files and the launcher...
    rm $HOME/.config/gambas3/scripted.conf  # scripted main settings
    rm -rf $HOME/.config/scripted  # scripted data, recents external tools, macros, etc

    rm $HOME/.local/share/applications/scripted.desktop  # the system menu item

## Basic usage..

* Like most text editors basic text editing usage is as you would expect.
* Scripts can be run/tested at the click of a button.
* Hover over buttons for tooltips on what they do.
* External tools [../exernal-tools.md]

T.B.C.

**Snapshots**

### Test running a script...

<img src="http://bws.org.uk/images/ScriptED_com.png">
<p>
<hr>
<p>
### External tools editor and Keystroke recorder...

<img src="http://bws.org.uk/images/ScriptEd2-2.png">
