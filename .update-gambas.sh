#!/usr/bin/env bash

ErrorOut() {
  echo -n "$1" >/tmp/err
  [ -e "/tmp/hold" ] && rm /tmp/hold
  sleep 1
  exit 1
}

CheckErr() {
  [ $? -ne 0 ] && ErrorOut "$1"
}

SortPkgs() {
  for s in $PKGS; do
    [[ " $NP " != *"$s"* ]] && NP="$NP $s"
  done
  PKGS="$NP"
}

ctrl_c() {
  ErrorOut "\n** Ctrl-C was pressed, exiting..."
}


if [ "$ID" = "ubuntu" ]; then
  zenity --question --text="With Ubuntu based systems you can add the official launchpad gambas repository (PPA)\nAlternatively you can download the source and compile with autotools" \
  --ok-label="Use PPA to update apt" --cancel-label="Use Autotools to compile" 2>/dev/null

  if [ $? -eq 0 ]; then
    if [ -z $(cat /etc/apt/sources.list|grep gambas) ]&&[ -z $(ls /etc/apt/sources.list.d/*gambas* 2>/dev/null) ]; then
      echo "Adding gambas PPA to apt source list..."
      sudo add-apt-repository -y ppa:gambas-team/gambas3
      CheckErr "Cannot add repository"
    fi
    sudo apt-get update -y
    CheckErr "Cannot update apt"
    echo "Removing the old and installing the new..."
    sudo apt-get purge -y gambas3*
    CheckErr "Cannot remove old gambas"
    PKGS=$PKGS" gambas3-gb-highlight gambas3-gb-args gambas3-scripter"
    SortPkgs

    sudo apt-get install -y $PKGS

    CheckErr "Could not install new gambas"
    if [ -e "/tmp/hold" ]; then rm /tmp/hold ; fi
    exit
  fi
fi

PKGS=""
if [ -z $(which git) ]; then PKGS="git"; fi
if [ -z $(which wget) ]; then PKGS="$PKGS wget"; fi
if [ ! -z "$PKGS" ]; then
  echo "Need to install the following packages: $PKGS"
  sudo apt-get install $PKGS;
  CheckErr "Cannot install tools"
fi

CWD=$(pwd)

cd "$HOME"
if [ ! -e "$HOME/gambas-source" ]; then
git clone https://gitlab.com/gambas/gambas.git --branch=stable --depth=1 gambas-source
CheckErr "Cannot clone gambas repository"
fi
cd "gambas-source"
wget -O "./Install_Gambas.sh" https://gitlab.com/bsteers4/gambas/-/raw/bruces-patched/Install_Gambas.sh
CheckErr "Cannot get install script"

chmod +x ./Install_Gambas.sh

if [ "$ID" = "ubuntu" ]; then SYS="$ID-$UBUNTU_CODENAME"
elif [ "$ID" = "debian" ]; then SYS="$ID-$VERSION_CODENAME"
elif [ "$ID" = "manjaro" ]||[[ "$ID" = *"arch"* ]]; then SYS="archlinux"
elif [[ "$ID" = "opensuse"* ]]; then SYS="opensuse-tumbleweed"
elif [[ "$ID" = "fedora"* ]]; then SYS="fedora-latest"
fi

./Install_Gambas.sh $SYS I
CheckErr "Error failed to compile gambas"

if [ -e "/tmp/hold" ]; then rm /tmp/hold ; fi

